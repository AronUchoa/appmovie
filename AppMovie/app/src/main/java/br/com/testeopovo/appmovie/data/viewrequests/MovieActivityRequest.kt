package br.com.testeopovo.appmovie.data.viewrequests

import android.content.Context
import br.com.testeopovo.appmovie.data.callbacks.GetGenreListResponse
import br.com.testeopovo.appmovie.data.callbacks.GetPopularMoviesResponse
import br.com.testeopovo.appmovie.data.webclients.GetGenreListWebClient
import br.com.testeopovo.appmovie.data.webclients.GetPopularMoviesWebClient
import br.com.testeopovo.appmovie.model.Genre
import br.com.testeopovo.appmovie.model.Movie
import br.com.testeopovo.appmovie.model.PopularMovie
import br.com.testeopovo.appmovie.utils.Constants
import java.util.*

class MovieActivityRequest {

    var language = Locale.getDefault().toString()

    // GetPopularMovies
    fun requestPopularMovie(numberPage: Int, context: Context, mServiceListener: MovieActivityRequestListener) {


        GetPopularMoviesWebClient().getPopularMovies(context, Constants.apiKey, language, numberPage, "", object:
        GetPopularMoviesResponse<PopularMovie> {
            override fun success(response: PopularMovie) {
//                mServiceListener.returnMovieListener(response, numberPage)
                mServiceListener.returnMovieListener(response)
            }
        })

//        GetPopularMoviesWebClient().getPopularMovies(context, Constants.apiKey, language, numberPage, "", object :
//                GetPopularMoviesResponse<PopularMovie> {
//            override fun success(response: <Pop>) {
//                mServiceListener.returnMovieListener(response, numberPage)
//            }
//        })
    }

    // GetGenreList
    fun requestGenreList(context: Context, mServiceListener: MovieActivityRequestListener) {

        GetGenreListWebClient().getGenreList(context, Constants.apiKey, language,
                object : GetGenreListResponse<MutableList<Genre>> {
                    override fun success(response: MutableList<Genre>) {
                        mServiceListener.returnGenreListListener(response)
                    }
                })
    }

    interface MovieActivityRequestListener {

        fun returnMovieListener(returnPopularMovie : PopularMovie)
//        fun returnMovieListener(returnList: MutableList<Movie>, numberPage: Int)
        fun returnGenreListListener(returnList: MutableList<Genre>)
    }

}