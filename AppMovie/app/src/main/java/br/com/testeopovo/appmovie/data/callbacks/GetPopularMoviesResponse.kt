package br.com.testeopovo.appmovie.data.callbacks

interface GetPopularMoviesResponse<T> {
    fun success(response: T)
}