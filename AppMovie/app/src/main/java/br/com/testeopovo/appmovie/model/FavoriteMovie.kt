package br.com.testeopovo.appmovie.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


//class FavoriteMovie : Serializable {
//
//    var description: String? = null
//
//    var id: Int? = null
//
//    var title: String? = null
//
//    var posterImage: String? = null
//
//    var genreIds: List<Int>? = null
//
//}


class FavoriteMovie(val id: String, val title: String, val description: String, val posterImage: String, val genreIds: List<Int>)