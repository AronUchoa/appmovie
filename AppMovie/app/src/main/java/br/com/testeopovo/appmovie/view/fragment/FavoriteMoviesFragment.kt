package br.com.testeopovo.appmovie.view.fragment


import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import br.com.testeopovo.appmovie.R
import br.com.testeopovo.appmovie.data.viewrequests.FavoriteFragmentRequest
import br.com.testeopovo.appmovie.data.viewrequests.MovieActivityRequest
import br.com.testeopovo.appmovie.model.FavoriteMovie
import br.com.testeopovo.appmovie.model.Genre
import br.com.testeopovo.appmovie.utils.Utils
import br.com.testeopovo.appmovie.view.adapter.FavoriteMovieAdapter
import br.com.testeopovo.appmovie.view.adapter.MovieAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_favorite_movies.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FavoriteMoviesFragment : Fragment(), FavoriteFragmentRequest.MovieActivityRequestListener {

    private val mAuth = FirebaseAuth.getInstance()

    lateinit var ref: DatabaseReference
    lateinit var favoriteMoviesList: MutableList<FavoriteMovie>
    private var genreList: MutableList<Genre> = mutableListOf()

    private var favoriteMovieRecyclerView: RecyclerView? = null
    private var adapter: FavoriteMovieAdapter? = null

    private val favoriteFragmentRequest = FavoriteFragmentRequest()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_favorite_movies, container, false)

        favoriteMoviesList = mutableListOf()

        // Configure the RecyclerView
        favoriteMovieRecyclerView = view.findViewById(R.id.frag_favorite_movies_recyclerView)
        val linearLayoutManager = LinearLayoutManager(this.activity, LinearLayout.VERTICAL, false)
        favoriteMovieRecyclerView!!.layoutManager = linearLayoutManager

        return view
    }

    override fun onResume() {
        super.onResume()

        favoriteMoviesList.clear()
        genreList.clear()
        if (frag_favorite_movies_empty_list != null) {
            frag_favorite_movies_empty_list.visibility = View.VISIBLE
        }
        frag_favorite_movies_progressBar.visibility = View.VISIBLE
        requestGenreList()
    }


    private fun getFavoriteMovies() {

        frag_favorite_movies_progressBar.visibility = View.GONE

        val query = FirebaseDatabase.getInstance().getReference("favoritemovies").child(mAuth.currentUser!!.uid)
        query.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {

                if (frag_favorite_movies_empty_list != null) {
                    frag_favorite_movies_empty_list.visibility = View.GONE
                }

                if (frag_favorite_movies_progressBar != null) {
                    frag_favorite_movies_progressBar.visibility = View.VISIBLE
                }

                Log.d("title", dataSnapshot.child("title").value!!.toString())

                val favoriteMovie = FavoriteMovie(

                        dataSnapshot.child("id").value!!.toString(),
                        dataSnapshot.child("title").value!!.toString(),
                        dataSnapshot.child("description").value!!.toString(),
                        dataSnapshot.child("posterImage").value!!.toString(),
                        (dataSnapshot.child("genreIds").value!! as? List<Int>)!!
                )

                favoriteMoviesList.add(favoriteMovie)

                adapter!!.notifyDataSetChanged()

                if (frag_favorite_movies_progressBar != null) {
                    frag_favorite_movies_progressBar.visibility = View.GONE
                }

            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {}

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {}

            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun requestFavoriteMovies() {

        ref = FirebaseDatabase.getInstance().getReference("favoritemovies")

        ref.addListenerForSingleValueEvent (object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

                Utils().showDialogWithoutCancel(context!!, context!!.getString(R.string.error),
                        context!!.getString(R.string.empty_movies), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })
            }

            override fun onDataChange(p0: DataSnapshot) {

                if (p0.exists()) {

                    getFavoriteMovies()
                }
            }

        })

        if (frag_favorite_movies_progressBar != null) {
            frag_favorite_movies_progressBar.visibility = View.GONE
        }
    }

    private fun requestGenreList() {

        favoriteFragmentRequest.requestGenreList(this.activity!!,this)
    }

    override fun returnGenreListListener(returnList: MutableList<Genre>) {

        genreList = returnList
        requestFavoriteMovies()

        adapter = FavoriteMovieAdapter(this.activity!!, favoriteMoviesList, genreList)
        favoriteMovieRecyclerView!!.adapter = adapter
    }
}
