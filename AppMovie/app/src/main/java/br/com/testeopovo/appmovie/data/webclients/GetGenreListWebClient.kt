package br.com.testeopovo.appmovie.data.webclients

import android.content.Context
import android.content.DialogInterface
import android.util.Log
import br.com.testeopovo.appmovie.data.RetrofitClientTMDB
import br.com.testeopovo.appmovie.data.callbacks.GetGenreListResponse
import br.com.testeopovo.appmovie.model.Genre
import br.com.testeopovo.appmovie.model.GenreList
import br.com.testeopovo.appmovie.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import br.com.testeopovo.appmovie.R

class GetGenreListWebClient {

    fun getGenreList(context: Context, apiKey: String, language: String,
                     callbackResponse: GetGenreListResponse<MutableList<Genre>>) {

        val call = RetrofitClientTMDB().getGenreListService().getGenreList(apiKey, language)
        call.enqueue(object: Callback<GenreList> {
            override fun onResponse(call: Call<GenreList?>?,
                                    response: Response<GenreList?>?) {

                Log.d("sucesso", "sucesso")
                response?.body().let {

                    val genreList: GenreList = it!!
                    val results = genreList.genres
                    callbackResponse.success((results as MutableList<Genre>?)!!)
                }
            }

            override fun onFailure(call: Call<GenreList?>?,
                                   t: Throwable?) {

                Log.e("onFailure error", t?.message)
                Utils().showDialogWithoutCancel(context,context.getString(R.string.error),
                        context.getString(R.string.message_error), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })
            }
        })
    }
}