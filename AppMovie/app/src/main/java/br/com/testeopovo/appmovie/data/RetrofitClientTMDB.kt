package br.com.testeopovo.appmovie.data

import br.com.testeopovo.appmovie.data.services.GetGenreListService
import br.com.testeopovo.appmovie.data.services.GetPopularMoviesService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClientTMDB {

    private val retrofit = Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    fun getGenreListService() = retrofit.create(GetGenreListService::class.java)

    fun getPopularMoviesService() = retrofit.create(GetPopularMoviesService::class.java)
}