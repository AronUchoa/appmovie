package br.com.testeopovo.appmovie.data.callbacks

interface GetGenreListResponse<T> {
    fun success(response: T)
}