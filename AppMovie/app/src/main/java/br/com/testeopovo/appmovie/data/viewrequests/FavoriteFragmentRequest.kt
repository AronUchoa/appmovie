package br.com.testeopovo.appmovie.data.viewrequests

import android.content.Context
import br.com.testeopovo.appmovie.data.callbacks.GetGenreListResponse
import br.com.testeopovo.appmovie.data.webclients.GetGenreListWebClient
import br.com.testeopovo.appmovie.model.Genre
import br.com.testeopovo.appmovie.utils.Constants
import java.util.*

class FavoriteFragmentRequest {

    var language = Locale.getDefault().toString()

    // GetGenreList
    fun requestGenreList(context: Context, mServiceListener: MovieActivityRequestListener) {

        GetGenreListWebClient().getGenreList(context, Constants.apiKey, language,
                object : GetGenreListResponse<MutableList<Genre>> {
                    override fun success(response: MutableList<Genre>) {
                        mServiceListener.returnGenreListListener(response)
                    }
                })
    }

    interface MovieActivityRequestListener {

        fun returnGenreListListener(returnList: MutableList<Genre>)
    }
}