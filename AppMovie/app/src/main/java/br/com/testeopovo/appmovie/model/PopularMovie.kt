package br.com.testeopovo.appmovie.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PopularMovie : Serializable {

    var results: ArrayList<Movie>? = null

    var page: Int? = null

    @SerializedName("total_results")
    var totalResults: Int? = null

    @SerializedName("total_pages")
    var totalPages: Int? = null
}