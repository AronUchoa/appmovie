package br.com.testeopovo.appmovie.data.preferences

import android.content.Context
import android.content.SharedPreferences
import br.com.testeopovo.appmovie.utils.Constants


class AppPreferences (context: Context) {

    private val PREFS_FILENAME = "br.com.testeopovo.appmovie.prefs"

    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)

    var userName: String
        get() = prefs.getString(Constants.USER_NAME, "")
        set(value) = prefs.edit().putString(Constants.USER_NAME, value).apply()

    var userEmail: String
        get() = prefs.getString(Constants.USER_EMAIL, "")
        set(value) = prefs.edit().putString(Constants.USER_EMAIL, value).apply()
}