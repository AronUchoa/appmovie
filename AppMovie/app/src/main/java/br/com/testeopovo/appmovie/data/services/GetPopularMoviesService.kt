package br.com.testeopovo.appmovie.data.services

import br.com.testeopovo.appmovie.model.PopularMovie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GetPopularMoviesService {

    @GET("movie/popular")
    fun getPopularMovies(@Query("api_key") apiKey: String,
                        @Query("language") language: String,
                        @Query("page") page: Int,
                        @Query("region") region: String) : Call<PopularMovie>
}