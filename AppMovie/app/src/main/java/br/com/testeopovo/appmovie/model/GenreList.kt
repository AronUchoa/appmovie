package br.com.testeopovo.appmovie.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class GenreList : Serializable {

    @SerializedName("genres")
    var genres: List<Genre>? = null
}