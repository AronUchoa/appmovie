package br.com.testeopovo.appmovie.view.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.TextView
import br.com.testeopovo.appmovie.R
import br.com.testeopovo.appmovie.data.preferences.AppPreferences
import br.com.testeopovo.appmovie.utils.Utils
import br.com.testeopovo.appmovie.view.fragment.FavoriteMoviesFragment
import br.com.testeopovo.appmovie.view.fragment.MoviesFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val mAuth = FirebaseAuth.getInstance()

    private var currentFragment: Fragment? = null
    private var ft: FragmentTransaction? = null

    private var userNameText: TextView? = null
    private var userEmailText: TextView? = null

    private var prefs: AppPreferences? = null

    lateinit var ref: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        prefs = AppPreferences(this)

        val header = nav_view.getHeaderView(0)
        userNameText = header.findViewById(R.id.nav_header_user_name)
        userEmailText = header.findViewById(R.id.nav_header_user_email)

//        userNameText!!.text = prefs!!.userName

        title = "Filmes Populares"
        ft = supportFragmentManager.beginTransaction()
        currentFragment = MoviesFragment()
        ft!!.replace(R.id.fragmentContainer, currentFragment)
        ft!!.commit()

        nav_view.setNavigationItemSelectedListener(this)

        requestUserInformations(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_popular_movies -> {

                title = "Filmes Populares"
                ft = supportFragmentManager.beginTransaction()
                currentFragment = MoviesFragment()
                ft!!.replace(R.id.fragmentContainer, currentFragment)
                ft!!.commit()
            }
            R.id.nav_favorite_movies -> {

                title = "Filmes Favoritos"
                ft = supportFragmentManager.beginTransaction()
                currentFragment = FavoriteMoviesFragment()
                ft!!.replace(R.id.fragmentContainer, currentFragment)
                ft!!.commit()
            }
            R.id.nav_logout -> {

                logout()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    private fun logout() {

        mAuth.signOut()

        val intent = Intent(this@MainActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun requestUserInformations(context: Context) {

        ref = FirebaseDatabase.getInstance().getReference("users")

        ref.addListenerForSingleValueEvent (object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

                Utils().showDialogWithoutCancel(context, context.getString(R.string.error),
                        context.getString(R.string.message_error), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })
            }

            override fun onDataChange(p0: DataSnapshot) {

                if (p0.exists()) {

                    getUserInformations()
                }
            }

        })
    }

    private fun getUserInformations() {

        val query = FirebaseDatabase.getInstance().getReference("users").child(mAuth.currentUser!!.uid)
        query.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {

                if (dataSnapshot.key == "name") {

                    prefs!!.userName = dataSnapshot.value.toString()
                    userNameText!!.text = prefs!!.userName
                }

                if (dataSnapshot.key == "email") {

                    prefs!!.userEmail = dataSnapshot.value.toString()
                    userEmailText!!.text = prefs!!.userEmail
                }

            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {}

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {}

            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}

            override fun onCancelled(databaseError: DatabaseError) {}
        })

    }
}