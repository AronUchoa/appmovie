package br.com.testeopovo.appmovie.model

class User (val email: String, val id: String, val imageUrl: String, val name: String, val phone: String)