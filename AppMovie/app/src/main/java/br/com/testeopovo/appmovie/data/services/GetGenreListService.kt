package br.com.testeopovo.appmovie.data.services

import br.com.testeopovo.appmovie.model.GenreList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface GetGenreListService {

    @GET("genre/movie/list")
    fun getGenreList(@Query("api_key") apiKey: String,
                     @Query("language") language: String) : Call<GenreList>
}