package br.com.testeopovo.appmovie.view.activity

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import br.com.testeopovo.appmovie.R
import br.com.testeopovo.appmovie.model.User
import br.com.testeopovo.appmovie.utils.GenericMask
import br.com.testeopovo.appmovie.utils.Utils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    private val mAuth = FirebaseAuth.getInstance()
    private val mFireBaseData = FirebaseDatabase.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        act_register_btnSignUp.setOnClickListener(this)

        act_register_txt_phone.addTextChangedListener(GenericMask.insert("(##)#####-####", act_register_txt_phone))
    }

    override fun onClick(v: View?) {

        if (v !== null){
            when(v.id) {
                R.id.act_register_btnSignUp -> {

                    act_register_progressBar.visibility = View.VISIBLE

                    signup(act_register_txt_email.text.toString(), act_register_txt_password.text.toString(),
                            act_register_txt_confirm_password.text.toString())
                }
            }
        }
    }

    fun signup(email: String, password: String, confirmPassword: String) {

        if (password != confirmPassword) {
            act_register_progressBar.visibility = View.GONE
            Utils().showDialogWithoutCancel(this, this.getString(R.string.error),
                    this.getString(R.string.different_password), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })
            return
        }

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->

            if (task.isSuccessful) {

                val reference = mFireBaseData.getReference("users")
                val user = User(email, mAuth.currentUser!!.uid,"", act_register_txt_name.text.toString(),
                        act_register_txt_phone.text.toString())

                reference.child(mAuth.currentUser!!.uid).setValue(user).addOnCompleteListener {

                    act_register_progressBar.visibility = View.GONE

                    val intent = Intent(this@RegisterActivity, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                }

            } else {

                act_register_progressBar.visibility = View.GONE

                Log.d("Error", task.exception!!.message)

                //TODO: Chamar Dialog para informar o user
                if (task.exception!!.message!!.contains("The email address is already in use by another account.")) {

                    Utils().showDialogWithoutCancel(this, this.getString(R.string.error),
                            this.getString(R.string.email_registered), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })
                } else if (task.exception!!.message!!.contains("The email address is badly formatted."))  {

                    Utils().showDialogWithoutCancel(this, this.getString(R.string.error),
                            this.getString(R.string.invalid_email), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })

                } else if (task.exception!!.message!!.contains("The given password is invalid. [ Password should be at least 6 characters ]")) {

                    Utils().showDialogWithoutCancel(this, this.getString(R.string.error),
                            this.getString(R.string.small_password), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })
                }
            }
        }
    }
}
