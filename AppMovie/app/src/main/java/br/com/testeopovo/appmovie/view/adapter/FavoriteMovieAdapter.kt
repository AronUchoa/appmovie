package br.com.testeopovo.appmovie.view.adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.testeopovo.appmovie.R
import br.com.testeopovo.appmovie.model.FavoriteMovie
import br.com.testeopovo.appmovie.model.Genre
import br.com.testeopovo.appmovie.model.Movie
import br.com.testeopovo.appmovie.utils.Constants
import br.com.testeopovo.appmovie.utils.Utils
import br.com.testeopovo.appmovie.view.activity.MovieDetailActivity
import de.hdodenhof.circleimageview.CircleImageView

class FavoriteMovieAdapter(private val activity: Activity, private val favoriteMovieList: MutableList<FavoriteMovie>, private val genreList: MutableList<Genre>) : RecyclerView.Adapter<FavoriteMovieAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {

        return favoriteMovieList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.txtMovieName.text = favoriteMovieList[position].title

        var genres = ""
        for (id in favoriteMovieList[position].genreIds) {
            for (item in genreList){
                if (id == item.id){
                    genres += item.name!! + ", "
                }
            }
        }
        if (genres.length > 2){
            val genreToLabel = genres.substring(0, genres.length - 2)
            val genreText = activity.getString(R.string.genre) + genreToLabel
            holder.txtGenre.text = genreText
        }else{
            val genreText = activity.getString(R.string.genre) + activity.getString(R.string.unspecified_genre)
            holder.txtGenre.text = genreText
        }

        Utils().glideLoadCircleImage(activity, favoriteMovieList[position].posterImage, holder.imageMovie)

        holder.itemView!!.setOnClickListener {

            val mMovie = Movie()

            mMovie.id = favoriteMovieList[position].id.toInt()
            mMovie.title = favoriteMovieList[position].title
            mMovie.overview = favoriteMovieList[position].description
            mMovie.posterPath = favoriteMovieList[position].posterImage


//            val id: String, val title: String, val description: String, val posterImage: String, val genreIds: List<Int>

            val intent = Intent(activity, MovieDetailActivity::class.java)
            intent.putExtra("favoriteIntent", true)
            intent.putExtra(Constants.movieDetailIntent, mMovie)
            activity.startActivity(intent)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val txtMovieName = itemView.findViewById<TextView>(R.id.item_movie_lbl_name)!!
        val imageMovie = itemView.findViewById<CircleImageView>(R.id.item_movie_image_movie)!!
        val txtGenre = itemView.findViewById<TextView>(R.id.item_movie_lbl_genre)!!
        val txtNoImage = itemView.findViewById<TextView>(R.id.item_movie_without_image_movie)
    }

}