package br.com.testeopovo.appmovie.view.adapter

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.testeopovo.appmovie.model.Movie
import br.com.testeopovo.appmovie.R
import br.com.testeopovo.appmovie.model.Genre
import br.com.testeopovo.appmovie.utils.Constants
import br.com.testeopovo.appmovie.utils.Utils
import br.com.testeopovo.appmovie.view.activity.MovieDetailActivity
import de.hdodenhof.circleimageview.CircleImageView

class MovieAdapter(private val activity: Activity, private val movieList: List<Movie>, private val genreList: MutableList<Genre>) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {

        return movieList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.txtMovieName.text = movieList[position].originalTitle

        if (movieList[position].backdropPath == null){
            if (movieList[position].posterPath == null) {
                holder.txtNoImage.visibility = View.VISIBLE
            }else {
                Utils().glideLoadCircleImage(activity, Constants.imageUrl +
                        movieList[position].posterPath, holder.imageMovie)
            }
        }else {
            Utils().glideLoadCircleImage(activity, Constants.imageUrl +
                    movieList[position].backdropPath, holder.imageMovie)
        }

        var genres = ""
        for (id in movieList[position].genreIds!!) {
            for (item in genreList){
                if (id == item.id){
                    genres += item.name!! + ", "
                }
            }
        }
        if (genres.length > 2){
            val genreToLabel = genres.substring(0, genres.length - 2)
            val genreText = activity.getString(R.string.genre) + genreToLabel
            holder.txtGenre.text = genreText
        }else{
            val genreText = activity.getString(R.string.genre) + activity.getString(R.string.unspecified_genre)
            holder.txtGenre.text = genreText
        }

        holder.itemView!!.setOnClickListener {

            val intent = Intent(activity, MovieDetailActivity::class.java)
            intent.putExtra(Constants.movieDetailIntent, movieList[position])
            activity.startActivity(intent)
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val txtMovieName = itemView.findViewById<TextView>(R.id.item_movie_lbl_name)!!
        val imageMovie = itemView.findViewById<CircleImageView>(R.id.item_movie_image_movie)!!
        val txtGenre = itemView.findViewById<TextView>(R.id.item_movie_lbl_genre)!!
        val txtNoImage = itemView.findViewById<TextView>(R.id.item_movie_without_image_movie)
    }

}