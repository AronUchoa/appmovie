package br.com.testeopovo.appmovie.view.activity

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import br.com.testeopovo.appmovie.R
import br.com.testeopovo.appmovie.data.preferences.AppPreferences
import br.com.testeopovo.appmovie.utils.Utils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private val mAuth = FirebaseAuth.getInstance()

    lateinit var ref: DatabaseReference
    var prefs: AppPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        act_login_btnAuthentication.setOnClickListener(this)
        act_login_btnGoSignup.setOnClickListener(this)

        prefs = AppPreferences(this)
    }

    override fun onClick(v: View?) {

        if (v != null) {
            when(v.id) {
                R.id.act_login_btnAuthentication -> {

                    act_login_progressBar.visibility = View.VISIBLE

                    signin(act_login_edtEmail.text.toString(), act_login_edtPass.text.toString())
                }

                R.id.act_login_btnGoSignup -> {

                    val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
                    startActivity(intent)
                }
            }
        }
    }

    fun signin(email: String, password: String) {

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->

            if (task.isSuccessful) {

                act_login_progressBar.visibility = View.GONE

                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            } else {

                act_login_progressBar.visibility = View.GONE
                Utils().showDialogWithoutCancel(this, this.getString(R.string.error),
                        this.getString(R.string.message_error), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })
            }
        }
    }
}
