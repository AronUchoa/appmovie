package br.com.testeopovo.appmovie.view.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import br.com.testeopovo.appmovie.R
import br.com.testeopovo.appmovie.data.viewrequests.MovieActivityRequest
import br.com.testeopovo.appmovie.model.Genre
import br.com.testeopovo.appmovie.model.Movie
import br.com.testeopovo.appmovie.model.PopularMovie
import br.com.testeopovo.appmovie.view.adapter.MovieAdapter
import kotlinx.android.synthetic.main.fragment_movies.*
import br.com.testeopovo.appmovie.utils.EndlessRecyclerViewScrollListener


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MoviesFragment : Fragment(), MovieActivityRequest.MovieActivityRequestListener {

    // Store a member variable for the listener
    private var scrollListener: EndlessRecyclerViewScrollListener? = null

    private val movieActivityRequest = MovieActivityRequest()

    private var movieRecyclerView: RecyclerView? = null
    private var movieList: MutableList<Movie> = mutableListOf()
    private var genreList: MutableList<Genre> = mutableListOf()
    private var adapter: MovieAdapter? = null

    private var firstPage = 1
    private var currentPage = 1
    private var totalPages = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_movies, container, false)

        requestGenreList()

        // Configure the RecyclerView
        movieRecyclerView = view.findViewById(R.id.frag_movies_recyclerView)
        val linearLayoutManager = LinearLayoutManager(this.activity, LinearLayout.VERTICAL, false)
        movieRecyclerView!!.layoutManager = linearLayoutManager

        // Retain an instance so that you can call `resetState()` for fresh searches
        scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                if (currentPage <= totalPages) {
                    requestPopularMovies(page+firstPage)
                }
            }
        }
        // Adds the scroll listener to RecyclerView
        movieRecyclerView!!.addOnScrollListener(scrollListener)

        return view
    }

    private fun requestPopularMovies(numberPage: Int) {

        movieActivityRequest.requestPopularMovie(numberPage, this.activity!!, this)
    }

    private fun requestGenreList() {

        movieActivityRequest.requestGenreList(this.activity!!,this)
    }

    override fun returnMovieListener(returnPopularMovie: PopularMovie) {

        Log.d("page", returnPopularMovie.page.toString())
        Log.d("total pages", returnPopularMovie.totalPages.toString())
        currentPage = returnPopularMovie.page!!
        totalPages = returnPopularMovie.totalPages!!

        if (returnPopularMovie.page == firstPage) {
            frag_movies_progressBar.visibility = View.GONE
        }
        for (item in returnPopularMovie.results!!) {
            movieList.add(item)
        }
        adapter?.notifyDataSetChanged()
    }

    override fun returnGenreListListener(returnList: MutableList<Genre>) {

        frag_movies_progressBar.visibility = View.GONE

        genreList = returnList
        requestPopularMovies(firstPage)

        adapter = MovieAdapter(this.activity!!, movieList, genreList)
        movieRecyclerView!!.adapter = adapter

    }
}
