package br.com.testeopovo.appmovie.data.webclients

import android.content.Context
import android.content.DialogInterface
import android.util.Log
import br.com.testeopovo.appmovie.data.RetrofitClientTMDB
import br.com.testeopovo.appmovie.data.callbacks.GetPopularMoviesResponse
import br.com.testeopovo.appmovie.model.Movie
import br.com.testeopovo.appmovie.model.PopularMovie
import br.com.testeopovo.appmovie.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import br.com.testeopovo.appmovie.R


class GetPopularMoviesWebClient {

    fun getPopularMovies(context: Context, apiKey: String, language: String, page: Int, region: String,
                         callbackResponse: GetPopularMoviesResponse<PopularMovie>) {

        val call = RetrofitClientTMDB().getPopularMoviesService().getPopularMovies(apiKey, language, page, region)
        call.enqueue(object : Callback<PopularMovie> {
            override fun onFailure(call: Call<PopularMovie>, t: Throwable) {

                Log.e("onFailure error", t.message)
                Utils().showDialogWithoutCancel(context,context.getString(R.string.error),
                        context.getString(R.string.message_error), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })
            }

            override fun onResponse(call: Call<PopularMovie>, response: Response<PopularMovie>) {

                Log.d("sucesso", "sucesso")
                response.body().let {

                    val movie: PopularMovie = it!!
//                    val results = movie.results
                    callbackResponse.success(movie)
                }
            }

        })

    }

}