package br.com.testeopovo.appmovie.view.activity

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import br.com.testeopovo.appmovie.R
import br.com.testeopovo.appmovie.model.FavoriteMovie
import br.com.testeopovo.appmovie.model.Movie
import br.com.testeopovo.appmovie.utils.Constants
import br.com.testeopovo.appmovie.utils.Utils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_movie_detail.*

class MovieDetailActivity : AppCompatActivity() {

    private var mMovie: Movie? = null
    private var mMenu: Menu? = null

    private val mAuth = FirebaseAuth.getInstance()
    private val mFireBaseData = FirebaseDatabase.getInstance()

    lateinit var ref: DatabaseReference
    lateinit var favoriteMoviesList: MutableList<FavoriteMovie>

    private var isFavoriteMovie = false
    private var genreList : MutableList<String>? = mutableListOf()

    private var isFavoriteIntent = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        setSupportActionBar(toolbar)
        displayHomeAsUpEnabled()
        toolbar.setOnClickListener {
            finish()
        }

        mMovie = intent.extras.get(Constants.movieDetailIntent) as? Movie
        isFavoriteIntent = intent.getBooleanExtra("favoriteIntent", false)

//        populateMovieDetail()

        verifyIfIsFavoriteMovie(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item!!.itemId) {
            android.R.id.home -> finish()

            R.id.menuFavorite -> {

                favoriteMovie()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.favorite_menu, menu)
        mMenu = menu
        return super.onCreateOptionsMenu(menu)
    }

    private fun displayHomeAsUpEnabled() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun populateMovieDetail() {

        act_movie_detail_progressBar.visibility = View.GONE


        if (isFavoriteIntent) {

            Utils().glideLoadCircleImage(this, mMovie!!.posterPath, act_movie_detail_image_movie)

        } else {
            Utils().glideLoadCircleImage(this, Constants.imageUrl + mMovie!!.posterPath, act_movie_detail_image_movie)

        }

        val movieName = getString(R.string.name) + mMovie!!.title
        val overview = mMovie!!.overview

        act_movie_detail_lbl_name.text = movieName
        act_movie_detail_lbl_title_overview.visibility = View.VISIBLE
        act_movie_detail_lbl_overview.text = overview
    }

    private fun favoriteMovie() {

        if (isFavoriteMovie) {

            removeFavoriteMovie()

        } else {

            val reference = mFireBaseData.getReference("favoritemovies")

            val favoriteMovie = FavoriteMovie(mMovie!!.id.toString(), mMovie!!.title.toString(),
                    mMovie!!.overview.toString(), Constants.imageUrl + mMovie!!.posterPath, mMovie!!.genreIds!!)

            reference.child(mAuth.currentUser!!.uid).child(mMovie!!.id.toString()).setValue(favoriteMovie).addOnCompleteListener {

                changeIconToFavorite()
            }

            //val referenceGenre = mFireBaseData.getReference("moviegenre")

            //genreList!!.clear()

//            for (genre in mMovie!!.genreIds!!) {
//
//                genreList!!.add(genre.toString())
//            }
//
//            referenceGenre.child(mAuth.currentUser!!.uid).child(mMovie!!.id.toString()).setValue(genreList)
        }

    }


    private fun changeIconToFavorite() {
        runOnUiThread {
            if (mMenu != null) {
                mMenu!!.findItem(R.id.menuFavorite)?.setIcon(R.drawable.baseline_favorite_white_24)
            }
        }
    }

    private fun changeIconToDisfavor() {
        runOnUiThread {
            if (mMenu != null) {
                mMenu!!.findItem(R.id.menuFavorite)?.setIcon(R.drawable.baseline_favorite_border_white_24)
            }
        }
    }

    private fun verifyIfIsFavoriteMovie(context: Context) {

        favoriteMoviesList = mutableListOf()

        ref = FirebaseDatabase.getInstance().getReference("favoritemovies")

        ref.addListenerForSingleValueEvent (object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

                populateMovieDetail()
                Utils().showDialogWithoutCancel(context, context.getString(R.string.error),
                        context.getString(R.string.empty_movies), DialogInterface.OnClickListener { dialog, i -> dialog.dismiss() })
            }

            override fun onDataChange(p0: DataSnapshot) {

                if (p0.exists()) {

                    getFavoriteMovies()
                }
            }

        })

        populateMovieDetail()
    }

    private fun getFavoriteMovies() {

        val query = FirebaseDatabase.getInstance().getReference("favoritemovies").child(mAuth.currentUser!!.uid)
        query.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {

                val favoriteMovie = FavoriteMovie(

                        dataSnapshot.child("id").value!!.toString(),
                        dataSnapshot.child("title").value!!.toString(),
                        dataSnapshot.child("description").value!!.toString(),
                        dataSnapshot.child("posterImage").value!!.toString(),
                        (dataSnapshot.child("genreIds").value!! as? List<Int>)!!
                )

                favoriteMoviesList.add(favoriteMovie)

                for (movie in favoriteMoviesList) {

                    if (movie.id.toInt() == mMovie!!.id) {
                        changeIconToFavorite()
                        isFavoriteMovie = true
                        return
                    }
                }

            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {}

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {}

            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}

            override fun onCancelled(databaseError: DatabaseError) {}
        })

    }

    private fun removeFavoriteMovie() {

        val reference = FirebaseDatabase.getInstance().getReference("favoritemovies").child(
                mAuth.currentUser!!.uid).child(mMovie!!.id.toString())
        reference.removeValue()

        changeIconToDisfavor()
        isFavoriteMovie = false
    }
}
