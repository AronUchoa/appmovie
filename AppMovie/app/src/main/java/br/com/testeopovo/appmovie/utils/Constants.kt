package br.com.testeopovo.appmovie.utils

object Constants {

    val imageUrl = "https://image.tmdb.org/t/p/w500"
    val apiKey = "9b4dafd86e57598cbb2eeaeb3194a8dd"
    val language = "en-US"

    val USER_NAME = "user_name"

    val USER_EMAIL = "user_email"

    val movieDetailIntent = "movie_detail_intent"

}