# AppMovie

Este app foi desenvolvido para mostrar os filmes populares, detalhes do filme popular e lista de filme favoritados pelo 
usuário. O código foi implementado em Kotlin e utilizei a versão 3.2 do Android Studio.

## Libraries and frameworks used on the sample project
AppCompat, CardView an DesignLibrary  
Retrofit 2 and Gson  
Glide  

Eu utilizei Retrofit para REST services.
Gson para serializar os modelos dos objetos.
CarView para uma visualização mais bonita no detalhe dos filmes
E Glide para carregar as imagens vinda do serivdor.


## Desenvolvido por
* Aron Uchôa

